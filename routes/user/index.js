const router = require("express").Router();
const { serializeUser } = require("../../Controllers/auth");
const { ROLE } = require("../../config/roles");

router.get("/profile", async (req, res) => {
  res.status(200).json({ type: ROLE.user, code: 200, data: serializeUser(req.user) });
});

module.exports = router;
